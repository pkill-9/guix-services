;; Maybe use separate caches for different architectures of glibc, so that binaries don't look for incorrect architectures? Then again I don't think it's a problem.
;; Maybe add -X to ldconfig command so it doesn't try to update links? As some fail, so this will get rid of those errors. And it's not supposed to add any links really, just register what already is there.
(define-module (pkill9 services)
  #:use-module (ice-9 ftw) ;; for creating recursive list of directories of libs for FHS  #:use-module (guix download)
  #:use-module (srfi srfi-1) ;; For "remove" for delete-service
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (guix gexp)
  #:use-module (guix build-system trivial)
  #:use-module (guix packages)
  #:use-module (guix git-download) ;; For steam-isolated-with-chrootenv
  #:use-module (gnu system accounts) ;; For 'user-account'
  #:use-module (gnu system shadow) ;; For 'account-service-type'
  #:use-module (gnu packages)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages base))

(define %steam-accounts
  (list  (user-account
         (name "test")
         (group "users")
         (supplementary-groups
          ;; Added to video group so hardware acceleration works, otherwise libgl complains about not having permissions.
          (list "netdev" "video"))
;;         (system? #t)
         (comment "For running untrusted software")
         (home-directory "/home/test")
         ;;(shell (file-append shadow "/sbin/nologin"))
         )))


(define-public steam-service-type
  (service-type (name 'steam)
                (extensions
                 (list (service-extension account-service-type
                                          (const %steam-accounts))
                       (service-extension profile-service-type
                                          list)))
                ))

(define (steam-service)
  "Add a wrapper to the system profile to run steam in a chroot (for certain required files in the root directory) and as it's own user."
  (display hello))
;;  (service steam-service-type))



(define-public (delete-service service-type services)
  (remove
   (lambda (service)
     (eq? (service-kind service) service-type))
   services))

(define (32bit-package pkg)
  (package (inherit pkg)
           (name (string-append (package-name pkg) "-i686-linux"))
	   (arguments
	    `(#:system "i686-linux"
	      ,@(package-arguments pkg)))))

;;=====================
;;====FHS support======
+;;Electorn apps are failing due to missing fonts, and fail catastrohpically and dont even give an error message other than "breakpoint trap"

(define glibc-for-fhs
  (package (inherit glibc)
           (name "glibc-for-fhs")
           (source (origin
                    (inherit (package-source glibc))
                    (snippet '(begin #t)))) ;;Re-enable ldconfig
           ))

;;define fhs-libs+ldconfig-settings, trivial-build-system:
;;create union of package libs in <this-package>/lib{32,64}
;;create <this-package>/etc/ld.so.conf with path to <this-package>/lib{32,64}
;;run <glibc-for-fhs>/sbin/ldconfig -f <this-package>/etc/ld.so.conf -C <this-package>/etc/ld.so.cache

(define fhs-libs
  (package
   (name "fhs-libs")
   (version "0")
   (source  #f)
   (build-system trivial-build-system)
   (arguments
    '(#:modules ((guix build union))
      #:builder
      (begin
        (use-modules (ice-9 match)
                     (guix build union))
        (let* ((out (assoc-ref %outputs "out")))
          (match %build-inputs
            (((names . directories) ...)
             (union-build (string-append out)
                          directories)))
	  ))))
   (inputs
    `(
      ("libxcomposite"
       ,(@ (gnu packages xorg) libxcomposite))
      ("libxtst" ,(@ (gnu packages xorg) libxtst))
      ("libxaw" ,(@ (gnu packages xorg) libxaw))
      ("libxt" ,(@ (gnu packages xorg) libxt))
      ("libxrandr" ,(@ (gnu packages xorg) libxrandr))
      ("libxext" ,(@ (gnu packages xorg) libxext))
      ("libx11" ,(@ (gnu packages xorg) libx11))
      ("libxfixes" ,(@ (gnu packages xorg) libxfixes))
      ("glib" ,(@ (gnu packages glib) glib))
      ("gtk+" ,(@ (gnu packages gtk) gtk+-2))
      ("gtk+" ,(@ (gnu packages gtk) gtk+))
      ("bzip2" ,(@ (gnu packages compression) bzip2))
      ("zlib" ,(@ (gnu packages compression) zlib))
      ("gdk-pixbuf" ,(@ (gnu packages gtk) gdk-pixbuf))
      ("libxinerama"
       ,(@ (gnu packages xorg) libxinerama))
      ("libxdamage"
       ,(@ (gnu packages xorg) libxdamage))
      ("libxcursor"
       ,(@ (gnu packages xorg) libxcursor))
      ("libxrender"
       ,(@ (gnu packages xorg) libxrender))
      ("libxscrnsaver"
       ,(@ (gnu packages xorg) libxscrnsaver))
      ("libxxf86vm"
       ,(@ (gnu packages xorg) libxxf86vm))
      ("libxi" ,(@ (gnu packages xorg) libxi))
      ("libsm" ,(@ (gnu packages xorg) libsm))
      ("libice" ,(@ (gnu packages xorg) libice))
      ("gconf" ,(@ (gnu packages gnome) gconf))
      ("freetype"
       ,(@ (gnu packages fontutils) freetype))
      ("curl" ,(@ (gnu packages curl) curl))
      ("nspr" ,(@ (gnu packages nss) nspr))
      ("nss" ,(@ (gnu packages nss) nss))
      ("fontconfig"
       ,(@ (gnu packages fontutils) fontconfig))
      ("cairo" ,(@ (gnu packages gtk) cairo))
      ("pango" ,(@ (gnu packages gtk) pango))
      ("expat" ,(@ (gnu packages xml) expat))
      ("dbus" ,(@ (gnu packages glib) dbus))
      ("cups" ,(@ (gnu packages cups) cups))
      ("libcap" ,(@ (gnu packages linux) libcap))
      ("sdl2" ,(@ (gnu packages sdl) sdl2))
      ("libusb" ,(@ (gnu packages libusb) libusb))
      ("dbus-glib" ,(@ (gnu packages glib) dbus-glib))
      ("atk" ,(@ (gnu packages gtk) atk))
      ("eudev" ,(@ (gnu packages linux) eudev))
      ("network-manager"
       ,(@ (gnu packages gnome) network-manager))
      ("gcc:lib" ,(@ (gnu packages gcc) gcc-7) "lib")
      ("pulseaudio"
       ,(@ (gnu packages pulseaudio) pulseaudio))
;;      ("libva" ,(@ (pkill9 packages steam additional-steam-libs libva) libva-1))
;;      ("libappindicator" ,(@ (pkill9 packages steam additional-steam-libs libappindicator) libappindicator)) ; for systray
      ("openal" ,(@ (gnu packages audio) openal))
      ("alsa-lib" ,(@ (gnu packages linux) alsa-lib))
      ("mesa" ,(@ (gnu packages gl) mesa))

      ("libxmu" ,(@ (gnu packages xorg) libxmu))
      ("libxcb" ,(@ (gnu packages xorg) libxcb))
      ("glu" ,(@ (gnu packages gl) glu))
      ("util-linux"
       ,(@ (gnu packages linux) util-linux))
      ("libogg" ,(@ (gnu packages xiph) libogg))
      ("libvorbis"
       ,(@ (gnu packages xiph) libvorbis))
      ("sdl" ,(@ (gnu packages sdl) sdl))
      ("sdl2-image" ,(@ (gnu packages sdl) sdl2-image))
      ("glew" ,(@ (gnu packages gl) glew))
      ("openssl" ,(@ (gnu packages tls) openssl-next))
      ("libidn" ,(@ (gnu packages libidn) libidn))
      ("tbb" ,(@ (gnu packages tbb) tbb))
      ("flac" ,(@ (gnu packages xiph) flac))
      ("freeglut" ,(@ (gnu packages gl) freeglut))
      ("libjpeg" ,(@ (gnu packages image) libjpeg))
      ("libpng" ,(@ (gnu packages image) libpng))
      ("libpng" ,(@ (gnu packages image) libpng-1.2))
      ("libsamplerate"
       ,(@ (gnu packages pulseaudio) libsamplerate))
      ("libmikmod" ,(@ (gnu packages sdl) libmikmod))
      ("libtheora" ,(@ (gnu packages xiph) libtheora))
      ("libtiff" ,(@ (gnu packages image) libtiff))
      ("pixman" ,(@ (gnu packages xdisorg) pixman))
      ("speex" ,(@ (gnu packages xiph) speex))
      ("sdl-image" ,(@ (gnu packages sdl) sdl-image))
      ("sdl-ttf" ,(@ (gnu packages sdl) sdl-ttf))
      ("sdl-mixer" ,(@ (gnu packages sdl) sdl-mixer))
      ("sdl2-ttf" ,(@ (gnu packages sdl) sdl2-ttf))
      ("sdl2-mixer" ,(@ (gnu packages sdl) sdl2-mixer))
      ("gstreamer"
       ,(@ (gnu packages gstreamer) gstreamer))
      ;!!TEST CURRENTLY FAILS ON i686-linux!!:
      ;("gst-plugins-base"
      ; ,(@ (gnu packages gstreamer) gst-plugins-base))
      ("glu" ,(@ (gnu packages gl) glu))
      ("libcaca" ,(@ (gnu packages video) libcaca))
      ("libcanberra"
       ,(@ (gnu packages libcanberra) libcanberra))
      ("libgcrypt" ,(@ (gnu packages gnupg) libgcrypt)) ;hmm, looks like duplicate
      ("libvpx" ,(@ (gnu packages video) libvpx))
      ("librsvg" ,(@ (gnu packages gnome) librsvg))
      ("libxft" ,(@ (gnu packages xorg) libxft))
      ("libvdpau" ,(@ (gnu packages video) libvdpau))
      ;("gst-plugins-ugly"
      ; ,(@ (gnu packages gstreamer) gst-plugins-ugly))
      ("libdrm" ,(@ (gnu packages xdisorg) libdrm))
      ("xkeyboard-config"
       ,(@ (gnu packages xorg) xkeyboard-config))
      ("libpciaccess"
       ,(@ (gnu packages xorg) libpciaccess))
      ("ffmpeg" ,(@ (gnu packages video) ffmpeg-3.4))
      ("libpng" ,(@ (gnu packages image) libpng-1.2))
;;      ("libgcrypt"
;;       ,(@ (pkill9 packages steam additional-steam-libs libgcrypt) libgcrypt-1.5.3))
      ("libgpg-error"
       ,(@ (gnu packages gnupg) libgpg-error))
      ("sqlite" ,(@ (gnu packages sqlite) sqlite))
      ("libnotify" ,(@ (gnu packages gnome) libnotify))

      ("fuse" ,(@ (gnu packages linux) fuse))
      ("e2fsprogs" ,(@ (gnu packages linux) e2fsprogs))
      ("p11-kit" ,(@ (gnu packages tls) p11-kit))
      ("xz" ,(@ (gnu packages compression) xz))
      ("keyutils" ,(@ (gnu packages crypto) keyutils))
      ("xcb-util-keysyms" ,(@ (gnu packages xorg) xcb-util-keysyms))
      ("libselinux" ,(@ (gnu packages selinux) libselinux))
      ("ncurses" ,(@ (gnu packages ncurses) ncurses))
      ("jack" ,(@ (gnu packages audio) jack-1))
      ("jack2" ,(@ (gnu packages audio) jack-2))
      
      ))
   (home-page #f)
   (synopsis "FHS libs union 64bit.")
   (description synopsis)
   (license #f)))

;; If possible, generate these files directly into the store, instead of into a directory in the store, i.e. instead of making them a package.
(define fhs-linker-config
  (package
   (name "fhs-linker-config")
   (version "0")
   (source  #f)
   (build-system trivial-build-system)
   (arguments
    '(#:modules ((guix build union)
                 (guix build utils))
      #:builder
      (begin
        (use-modules (ice-9 match)
                     (guix build union)
                     (guix build utils))
        (let* ((out (assoc-ref %outputs "out"))
               (ldconfig (string-append (assoc-ref %build-inputs "glibc-for-fhs") "/sbin/ldconfig"))
               (fhs-libs-64 (assoc-ref %build-inputs "fhs-libs-64"))
               (fhs-libs-32 (assoc-ref %build-inputs "fhs-libs-32")))
            (mkdir (string-append out))
            (mkdir (string-append out "/etc"))
            ;;Create ld.so.conf
            ;; Run this from the package root to generate recursive list of directories:
            ;;sh -c 'find -type d | cut -b 1-2 --complement | sed "s/^/<out-package>\//" > <out-package>/etc/ld.so.conf'
            (let ((fhs-lib-dirs (append
                                 (find-files (string-append fhs-libs-32 "/lib")
                                             (lambda (file stat)
                                               (eq? 'directory (stat:type stat)))
                                             #:stat stat
                                             #:directories? #t)
                                 (find-files (string-append fhs-libs-64 "/lib")
                                             (lambda (file stat)
                                               (eq? 'directory (stat:type stat)))
                                             #:stat stat ;; setting keyword "stat" to "stat" means it will follow symlinks, unlike what it's set to by default ("lstat")
                                             #:directories? #t))))

          (with-output-to-file
              (string-append out "/etc/ld.so.conf")
            (lambda _
              (format #t
                      (string-join (map (lambda (dir)
                                          (string-append dir
                                                         "\n"))
                                        fhs-lib-dirs)))
              ;;(string-append fhs-libs-64 "/lib" "\n"
              ;;               fhs-libs-32 "/lib")
              out)))
          ;;Run ldconfig to create ld.so.cache
          (invoke ldconfig
                  "-f" (string-append out "/etc/ld.so.conf")
                  "-C" (string-append out "/etc/ld.so.cache"))
	  ))))
   (native-inputs
    `(("glibc-for-fhs" ,glibc-for-fhs)))
   (inputs
    `(("fhs-libs-64" ,fhs-libs)
      ("fhs-libs-32" ,(32bit-package fhs-libs))))
   (home-page #f)
   (synopsis "ldconfig cache for FHS libraries.")
   (description synopsis)
   (license #f)))

;;define special-files-service-type
;;Need to have it add ldconfig to PATH, perhaps also desktop file utils, and other tools binaries use

(define-public %fhs-support-service
  (service special-files-service-type
           `(("/etc/ld.so.cache" ,(file-append (canonical-package fhs-linker-config) "/etc/ld.so.cache"))
             ("/etc/ld.so.conf" ,(file-append (canonical-package fhs-linker-config) "/etc/ld.so.conf")) ;;Not needed to function, but put it here anyway for debugging purposes
             ("/lib64/ld-linux-x86-64.so.2" ,(file-append (canonical-package glibc-for-fhs) "/lib/ld-linux-x86-64.so.2"))
             ("/lib/ld-linux.so.2" ,(file-append (canonical-package (32bit-package glibc-for-fhs)) "/lib/ld-linux.so.2"))
             ;;             ("/fhs/libs" ,(file-append (canonical-package fhs-libs-64) "/lib"))
             ("/usr/share/X11/xkb" ,(file-append (canonical-package (@ (gnu packages xorg) xkeyboard-config)) "/share/X11/xkb")) ;; QT apps fail to recieve keyboard input unless they find this hardcoded path.
             ("/etc/fonts" ,"/run/current-system/profile/etc/fonts") ;; Chromium component of electron apps break without fontconfig configuration here.
             )))
;;====FHS support======
;;=====================

;;=====================
;;====steam support====
;;
;;Hardcoded paths required by steam. (Also enables portable support for shebangs and binaries)
;;
;;Paths can't be modified because steam checks itself
;;  to see if it's been modified, and replaces itself
;;  if it has.
;;
;;Add %steam-support-service to services declaration
;;  to add these hardcoded paths.
(define-public %steam-support-service
  (service special-files-service-type
           `(("/usr/bin/env" ,(file-append (canonical-package coreutils) "/bin/env"))
             ("/bin/bash" ,(file-append (canonical-package bash) "/bin/bash"))
             ("/lib64/ld-linux-x86-64.so.2" ,(file-append (canonical-package glibc-for-fhs) "/lib/ld-linux-x86-64.so.2"))
             ("/lib/ld-linux.so.2" ,(file-append (32bit-package glibc-for-fhs) "/lib/ld-linux.so.2"))
                                        ;need both 32bit and 64bit glibc
             )))
;;====steam support====
;;=====================

(define-public %brightness-service
  ;; Run as a Shepherd service instead of an activation script since the
  ;; latter typically runs before all modules have been loaded.
  (simple-service 'brightness-service shepherd-root-service-type
                  (list (shepherd-service
                         (provision '(bash))
                         (requirement '())
                         (start #~(lambda ()
                                    (invoke
                                     #$(file-append bash "/bin/bash")
                                     "-c"
				     "echo 50 > /sys/class/backlight/intel_backlight/brightness")))
                         (respawn? #f)))))

(define-public %powertop-service
  ;; Run as a Shepherd service instead of an activation script since the
  ;; latter typically runs before all modules have been loaded.
  (simple-service 'powertop shepherd-root-service-type
                  (list (shepherd-service
                         (provision '(powertop))
                         (requirement '())
                         (start #~(lambda ()
                                    (invoke
                                     #$(file-append powertop "/sbin/powertop")
                                     "--auto-tune")))
                         (respawn? #f)))))

(define-public steam-isolated-with-chrootenv
  (package
   (name "steam-isolated-with-chrootenv")
   (version "0")
   (source (origin
             (method git-fetch)
             (uri (git-reference
                   (url "https://gitlab.com/pkill-9/steam-isolated-with-chrootenv.git")
                   (commit "eb42b02522f7903023b583d598b76e7b1d52831e")))
             (file-name (git-file-name name version))
             (sha256
              (base32
               "0pyd9193lpw9169qzmgmcdffxakpzvxsbrlsizr2djq8pyd8z6yj"))))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let* ((source (assoc-ref %build-inputs "source"))
               (out (assoc-ref %outputs "out")))
          (copy-recursively
           (string-append source)
           (string-append out "/bin"))
          )
        )))
   (home-page "")
   (synopsis "Steam run as another user, in a chroot")
   (description "")
   (license #f)))

;;(package (inherit fhs-linker-config))
(package (inherit steam-isolated-with-chrootenv))
